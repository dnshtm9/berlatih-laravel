<!DOCTYPE html>
<html>
<head>
<title>Form</title>
</head>
<body>

<h1>Buat Akun Baru!</h1>
<h2>Sign Up Form</h2>
<form action="/welcome" method='POST'>
    @csrf
    <label for="firstname">First Name:</label><br>
    <input type="text" id="firstname" name="firstname"><br>
    <label for="lastname">Last Name:</label><br>
    <input type="text" id="last" name="lastname"><br><br>
    <label for="gender">Gender:</label><br>
    <input type="radio" id="male" name="gender" value="male">
    <label for="male">Male</label><br>
    <input type="radio" id="female" name="gender" value="female">
    <label for="female">Female</label><br>
    <input type="radio" id="other" name="gender" value="other">
    <label for="other">Other</label><br><br>
    <label for="nationality">Nationality:</label><br>
    <select name="nationality" id="nationality">
        <option value="indonesian">Indonesian</option>
        <option value="singaporean">Singaporean</option>
        <option value="malaysian">Malaysian</option>
        <option value="australian">Australian</option>
    </select><br><br>
    <label for="language">Language spoken:</label><br>
    <input type="checkbox" id="bindo" name="bindo" value="bindo">
    <label for="bindo">Bahasa Indonesia</label><br>
    <input type="checkbox" id="binggris" name="binggris" value="binggris">
    <label for="binggris">Bahasa Inggris</label><br>
    <input type="checkbox" id="bother" name="bother" value="bother">
    <label for="bother">Other</label><br><br>
    <label for="bio">Bio:</label><br>
    <textarea name="bio"></textarea><br><br>
    <input type="submit" value="Sign Up">
</form>
</body>
</html>